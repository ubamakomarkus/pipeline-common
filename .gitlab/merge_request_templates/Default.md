## What does this MR do?

<!-- Briefly describe what this MR is about and why the change is needed -->

## Check-list

- [ ] Ensure commits include `Changelog:` [trailer](https://docs.gitlab.com/ee/development/changelog.html)
- [ ] Verify the change in [all affected pipelines](https://handbook.gitlab.com/handbook/engineering/infrastructure/test-platform/debugging-qa-test-failures/#qa-test-pipelines).

/label ~"type::maintenance" ~"maintenance::pipelines"

<!-- template sourced from https://gitlab.com/gitlab-org/quality/pipeline-common/-/blob/master/.gitlab/merge_request_templates/Default.md -->
