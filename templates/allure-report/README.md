## `allure-report` component

The component generates a job that creates an [Allure report from RSpec results](https://github.com/allure-framework/allure-ruby/tree/master/allure-rspec).

### Inputs

#### `job_name`

The name of the generated job.

#### `job_stage`

The stage of the generated job.

#### `aws_access_key_id_variable_name`

Variable **name** where the AWS access key ID is set. **Don't include the `$` prefix as it's added by the template.**

#### `aws_secret_access_key_variable_name`

Variable **name** where the AWS secret access key is set. **Don't include the `$` prefix as it's added by the template.**

#### `gitlab_auth_token_variable_name`

Variable **name** where the GitLab auth token with `api` access is set. **Don't include the `$` prefix as it's added by the template.**

#### `allure_results_glob`

Glob for allure results locations.

#### `allure_ref_slug`

The ref slug, used in the bucket prefix (default: `${TOP_UPSTREAM_SOURCE_REF:=$CI_COMMIT_REF_SLUG}`).

#### `allure_project_path`

Overrides current project path (default: `${CI_PROJECT_PATH}`).

#### `allure_merge_request_iid`

Overrides current merge request iid (default: `${CI_MERGE_REQUEST_IID}`).

#### `allure_job_name`

Job name for report MR comment and base folder in gcs bucket (default: `${CI_JOB_NAME}`).

#### `allure_mr_update_type`

MR update type (default: `comment`).

#### `allure_summary_table_type`

Summary table type (default: `ascii`).

